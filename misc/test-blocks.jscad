// title      : test block
// revision   : 0.0.0
// author     : 2john4tv
// license    : Free Public License 1.0.0
//              https://opensource.org/licenses/FPL-1.0.0

function main () {
  var size = [50, 10, 50];
  var radius = 2;
  var resolution = 48;

  return union(
    cube({radius: radius, size: size, fn: resolution }),
    cube({
      radius: [radius[0], radius[1], 0],
      size: [size[0], size[1], size[2] - radius[2]],
      fn: resolution
    })
  );
}
