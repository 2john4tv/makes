// title      : PSU cover
// revision   : 0.0.0
// author     : 2john4tv
// license    : Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
//              https://creativecommons.org/licenses/by-nc-sa/4.0/
//              https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

function main () {
  var wallThickness = 2;
  var slotSize = [115.5, 51, 72];

  var chassisSize = [
    slotSize[0] + (wallThickness * 2),
    slotSize[1] + (wallThickness * 2),
    slotSize[2]
  ];

  var slot = cube({size: slotSize});
  var chassis = cube({
    radius: [2, 2, 2],
    resolution: 48,
    size: chassisSize
  });

  chassis = difference(
    chassis,
    translate([wallThickness, wallThickness, wallThickness], slot)
  );

  var screwOffset = 4.5;
  var screwDiameter = 3;
  var powerPortSize = [47.5, 28.5, 5];
  var powerPortTranslation = [7, (chassisSize[1] - powerPortSize[1])/2, 0];

  var screwRadius = screwDiameter / 2;
  var screwHole = cylinder({h: 10, r: screwRadius, fn: 100});

  var powerPortHoles = union(
    cube({size: powerPortSize}),
    translate([
      powerPortSize[0]/2,
      powerPortSize[1] + screwOffset + screwRadius,
      0
    ], screwHole)
  );
  powerPortHoles = union(
    powerPortHoles,
    translate([
      powerPortSize[0]/2,
      (screwOffset * -1) - screwRadius,
      0
    ], screwHole)
  );

  var powerPort = cube({
    size: [
      powerPortSize[0] + wallThickness + powerPortTranslation[0],
      slotSize[1],
      5
    ]
  });

  chassis = union(
    chassis,
    translate([wallThickness, wallThickness, 0], powerPort)
  );

  chassis = difference(
    chassis,
    translate(powerPortTranslation, powerPortHoles)
  )

  var powerOutHole = {
    diameter: 7,
    translation: [chassisSize[0] - 40, chassisSize[1]/2, 0]
  }
  var powerOut = translate(
    powerOutHole.translation,
    cylinder({r: powerOutHole.diameter / 2,  h: wallThickness})
  )

  chassis = difference(chassis, powerOut);

  return translate([chassisSize[0]/-2, chassisSize[1]/-2, 0], chassis);
}
